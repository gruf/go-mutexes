package mutexes_test

import (
	"testing"
	"time"

	"codeberg.org/gruf/go-mutexes"
)

const timeout = time.Millisecond

func TestMutexTimeout(t *testing.T) {
	var timedOut bool

	mu := mutexes.WithTimeout(mutexes.New(), timeout)

	unlockFn := mu.LockFunc(func() { timedOut = true })
	time.Sleep(timeout * 10)
	unlockFn()

	if !timedOut {
		t.Fatal("Should have timed out")
	}
}

func TestRWMutexTimeout(t *testing.T) {
	var rTimedOut bool
	var wTimedOut bool

	mu := mutexes.WithTimeoutRW(mutexes.NewRW(), timeout, timeout)

	unlockFn := mu.RLockFunc(func() { rTimedOut = true })
	time.Sleep(timeout * 10)
	unlockFn()

	unlockFn = mu.LockFunc(func() { wTimedOut = true })
	time.Sleep(timeout * 2)
	unlockFn()

	if !rTimedOut || !wTimedOut {
		t.Fatal("Should have timed out")
	}
}

func TestMutexSafety(t *testing.T) {
	mu := mutexes.WithSafety(mutexes.New())
	unlockFn := mu.Lock()

	catchPanic(
		t,
		func() {
			// Defer unlock
			defer unlockFn()

			// Unlock early
			unlockFn()

			panic("panic! at the golang?")
		},
		func(v interface{}) {
			if v != nil && v != "panic! at the golang?" {
				t.Fatalf("Panic during unlock: %v", v)
			}
		},
	)
}

func TestRWMutexSafety(t *testing.T) {
	mu := mutexes.WithSafetyRW(mutexes.NewRW())

	// Try read lock
	unlockFn := mu.RLock()

	catchPanic(
		t,
		func() {
			// Defer unlock
			defer unlockFn()

			// Unlock early
			unlockFn()

			panic("panic! at the golang?")
		},
		func(v interface{}) {
			if v != nil && v != "panic! at the golang?" {
				t.Fatalf("Panic during unlock: %v", v)
			}
		},
	)

	// Try write lock
	unlockFn = mu.Lock()

	catchPanic(
		t,
		func() {
			// Defer unlock
			defer unlockFn()

			// Unlock early
			unlockFn()

			panic("panic! at the golang?")
		},
		func(v interface{}) {
			if v != nil && v != "panic! at the golang?" {
				t.Fatalf("Panic during unlock: %v", v)
			}
		},
	)
}

func TestMutexFunc(t *testing.T) {
	mu := mutexes.WithFunc(
		mutexes.New(),
		func() { t.Log("lock") },
		func() { t.Log("unlock") },
	)
	mu.Lock()()
}

func TestRWMutexFunc(t *testing.T) {
	mu := mutexes.WithFuncRW(
		mutexes.NewRW(),
		func() { t.Log("lock") },
		func() { t.Log("rlock") },
		func() { t.Log("unlock") },
		func() { t.Log("runlock") },
	)
	mu.RLock()()
	mu.Lock()()
}

func catchPanic(t *testing.T, do func(), onRecover func(v interface{})) {
	defer func() {
		r := recover()
		onRecover(r)
	}()
	do()
}
