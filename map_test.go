package mutexes_test

import (
	"strconv"
	"sync"
	"testing"
	"time"

	"codeberg.org/gruf/go-mutexes"
)

const hammerTestCount = 1000

func TestMapMutex(t *testing.T) {
	var (
		mm mutexes.MutexMap

		unlock func()

		runlock1 func()
		runlock2 func()
		runlock3 func()
	)

	// Check that write lock of one
	// key doesn't impede read locks.
	shouldHappenWhile(func() {
		unlock = mm.Lock("w-hello")
	}, func() {
		runlock1 = mm.RLock("r-hello")
		runlock2 = mm.RLock("r-hello")
		runlock3 = mm.RLock("r-hello")
	})

	unlock()
	runlock1()
	runlock2()
	runlock3()

	// Check we cannot get write lock twice.
	shouldNotHappenWhile(func() {
		unlock = mm.Lock("w-hello")
	}, func() {
		mm.Lock("w-hello")()
	})

	unlock()

	// Check that write lock of
	// key impedes read locks.
	shouldNotHappenWhile(func() {
		unlock = mm.Lock("hello")
	}, func() {
		mm.RLock("hello")()
	})
}

func shouldHappenWhile(while func(), should func(), timeout ...time.Duration) {
	sleep := time.Second

	if len(timeout) > 0 {
		sleep = timeout[0]
	}

	ch := make(chan struct{})

	while()

	go func() {
		should()
		close(ch)
	}()

	select {
	case <-ch:
	case <-time.After(sleep):
		panic("timed out")
	}
}

func shouldNotHappenWhile(while func(), shouldNot func(), timeout ...time.Duration) {
	sleep := time.Second

	if len(timeout) > 0 {
		sleep = timeout[0]
	}

	ch := make(chan struct{})

	while()

	go func() {
		shouldNot()
		close(ch)
	}()

	time.Sleep(sleep)

	select {
	case <-ch:
		panic("should not have happened")
	default:
	}
}

func TestMapMutexHammer(t *testing.T) {
	var mm mutexes.MutexMap

	wg := sync.WaitGroup{}
	for i := 0; i < hammerTestCount; i++ {
		go func(i int) {
			key := strconv.Itoa(i)

			// Get the original starting lock
			runlockBase := mm.RLock(key)

			// Perform slow async unlock
			wg.Add(1)
			go func() {
				time.Sleep(time.Second)
				runlockBase()
				wg.Done()
			}()

			// Perform a quick write lock
			mm.Lock(key)()

			for j := 0; j < hammerTestCount; j++ {
				runlock := mm.RLock(key)

				// Perform slow async unlock
				wg.Add(1)
				go func() {
					time.Sleep(time.Millisecond * 10)
					runlock()
					wg.Done()
				}()
			}

			// Start a waiting write lock
			unlock := mm.Lock(key)
			unlock()
		}(i)
	}
	time.Sleep(time.Second)
	wg.Wait()
}
