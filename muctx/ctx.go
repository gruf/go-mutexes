package muctx

import (
	"context"
	"sync/atomic"
	_ "unsafe"

	"codeberg.org/gruf/go-mutexes"
)

// Lock represents a locked key in a mutex map,
// normally only returned as an unlock function
// but wrapped here to provide a storable type
// with metadata for context values.
type Lock struct {
	unlk func()
	lock *uint32
	ltyp uint8
}

// RLockCtx will check given context for rlock, else acquire an rlock on the mutex at key in the map.
func RLockCtx(mm *mutexes.MutexMap, ctx context.Context, key string) (child context.Context, lock *Lock) {
	return lockCtx(mm, ctx, key, lockTypeRead)
}

// LockCtx will check given context for lock, else acquire a lock on the mutex at key in the map.
func LockCtx(mm *mutexes.MutexMap, ctx context.Context, key string) (child context.Context, lock *Lock) {
	return lockCtx(mm, ctx, key, lockTypeWrite)
}

func lockCtx(mm *mutexes.MutexMap, ctx context.Context, key string, lt uint8) (context.Context, *Lock) {
	type ctxkey string

	// Look for an existing Lock{} with key.
	lock, ok := ctx.Value(ctxkey(key)).(*Lock)
	if ok && lock.Locked() {
		if lock.ltyp&lt == 0 {
			panic("called (r)lock on ctx with different lock type for same key - deadlock!")
		}
		return ctx, lock.Clone()
	}

	// Alloc new Lock.
	lock = new(Lock)
	lock.lock = new(uint32)
	*lock.lock = 1 // locked
	lock.ltyp = lt

	// Acquire mutex map lock for key.
	lock.unlk = mutexmap_lock(mm, key, lt)

	// Wrap parent context with our LockCtx value.
	child := context.WithValue(ctx, ctxkey(key), lock)
	return child, lock
}

// Locked returns whether Lock is currently locked.
func (ctx *Lock) Locked() bool {
	return atomic.LoadUint32(ctx.lock) == 1
}

// Unlock will unlock the given Lock (only if it is not a read-only).
func (ctx *Lock) Unlock() bool {
	if ctx.unlk != nil &&
		atomic.CompareAndSwapUint32(ctx.lock, 1, 0) {
		ctx.unlk()
		return true
	}
	return false
}

// Clone returns a read-only clone of Lock.
func (ctx *Lock) Clone() *Lock {
	return &Lock{
		lock: ctx.lock,
		ltyp: ctx.ltyp,
	}
}

// IsRLock returns whether this Lock is a read lock.
func (ctx *Lock) IsRLock() bool {
	return ctx.ltyp&lockTypeRead != 0
}

// IsLock returns whether this Lock is a write lock.
func (ctx *Lock) IsLock() bool {
	return ctx.ltyp&lockTypeWrite != 0
}

const (
	// possible lock types.
	lockTypeRead  = uint8(1) << 0
	lockTypeWrite = uint8(1) << 1
)

//go:linkname mutexmap_lock codeberg.org/gruf/go-mutexes.(*MutexMap).lock
func mutexmap_lock(mm *mutexes.MutexMap, key string, lt uint8) func()
