package muctx_test

import (
	"context"
	"testing"

	"codeberg.org/gruf/go-mutexes"
	"codeberg.org/gruf/go-mutexes/muctx"
)

func TestMapLockCtx(t *testing.T) {
	var mm mutexes.MutexMap

	ctx := context.Background()

	ctx, lock := muctx.LockCtx(&mm, ctx, "hello")

	if !lock.Locked() || !lock.IsLock() || lock.IsRLock() {
		t.Fatal("lock was not locked / incorrect lock type")
	}

	ctx2, lock2 := muctx.LockCtx(&mm, ctx, "hello")

	if ctx != ctx2 {
		t.Fatal("returned new context for ctx with existing lock")
	}

	if !lock2.Locked() {
		t.Fatal("lock2 was not locked")
	}

	if lock2.Unlock() || !lock.Locked() || !lock2.Locked() {
		t.Fatal("call to lock2 should not succeed")
	}

	if !lock.Unlock() || lock.Locked() || lock2.Locked() {
		t.Fatalf("call to lock should succeed: %#v %#v", lock, lock2)
	}
}

func TestRLockCtx(t *testing.T) {
	var mm mutexes.MutexMap

	ctx := context.Background()

	ctx, lock := muctx.RLockCtx(&mm, ctx, "hello")

	if !lock.Locked() || lock.IsLock() || !lock.IsRLock() {
		t.Fatal("lock was not locked / incorrect lock type")
	}

	ctx2, lock2 := muctx.RLockCtx(&mm, ctx, "hello")

	if ctx != ctx2 {
		t.Fatal("returned new context for ctx with existing lock")
	}

	if !lock2.Locked() {
		t.Fatal("lock2 was not locked")
	}

	if lock2.Unlock() || !lock.Locked() || !lock2.Locked() {
		t.Fatal("call to lock2 should not succeed")
	}

	if !lock.Unlock() || lock.Locked() || lock2.Locked() {
		t.Fatalf("call to lock should succeed: %#v %#v", lock, lock2)
	}
}
